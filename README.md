
## Compiling

By letting PETSc figuring out the compiler (icc):
```bash
module load intel-oneapi-compilers/2023.0.0_gcc-10.4.0 petsc/3.18.3_gcc-10.4.0-intelmpi
mkdir build && cd build
cmake .. -DDETECT_COMPILER=ON
make
```

By using intel's mpicc but with the default compiler (gcc 10) under the hood:
```bash
module load petsc/3.18.3_gcc-10.4.0-intelmpi
mkdir build && cd build
cmake .. -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpicxx
make
```

## Running the example

On a single node:
```bash
mpiexec -n 1 ./ex50  -da_grid_x 4 -da_grid_y 4 -mat_view
```

On multiple nodes (`hosts` contains one host per line):
```bash
mpiexec -ppn 1 -f hosts ./ex50  -da_grid_x 120 -da_grid_y 120 -pc_type lu -pc_factor_mat_solver_type superlu_dist -ksp_monitor -ksp_view
```
